#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.69)
AC_INIT([biolite-tools], [0.4.0], [mhowison@brown.edu])
AC_CONFIG_HEADERS([config.h])

AM_INIT_AUTOMAKE
AM_SILENT_RULES([yes])

AC_CANONICAL_HOST

# Set default optimization to O3
if test -z $CXXFLAGS; then
    CXXFLAGS="-O3 -g3"
fi

# Checks for programs.
AC_PROG_MAKE_SET
AC_PROG_INSTALL
AC_PROG_RANLIB
AC_LANG([C++])
AC_PROG_CXX

# Checks for libraries.
# Add -lz here without test... its on pretty much any *nix system.
LIBS="$LIBS -lz"

# Checks for header files.
AC_CHECK_HEADERS([limits.h stdlib.h string.h unistd.h])

# Checks for hash implementations.
AC_CHECK_HEADERS([unordered_map tr1/unordered_map unordered_set tr1/unordered_set])

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_MALLOC
AC_CHECK_FUNCS([sqrt strrchr strtoul])

# Version information.
PACKAGE_COMPILER=`$CXX --version | head -1`
PACKAGE_COMPILER="$CXX (version: $PACKAGE_COMPILER)"
PACKAGE_BUILD_HOST=`uname -n`
PACKAGE_BUILD_HOST_OS=`uname -srm`
PACKAGE_BUILD_DATE=`date`

# Output.
AC_DEFINE_UNQUOTED([PACKAGE_COMPILER], ["$PACKAGE_COMPILER"],
	[Define for printing in version information.])
AC_DEFINE_UNQUOTED([PACKAGE_COMPILE_OPTIONS], ["$CXXFLAGS"],
	[Define for printing in version information.])
AC_DEFINE_UNQUOTED([PACKAGE_BUILD_HOST],["$PACKAGE_BUILD_HOST"],
	[Define for printing in version information.])
AC_DEFINE_UNQUOTED([PACKAGE_BUILD_HOST_OS], ["$PACKAGE_BUILD_HOST_OS"],
	[Define for printing in version information.])
AC_DEFINE_UNQUOTED([PACKAGE_BUILD_DATE], ["$PACKAGE_BUILD_DATE"],
	[Define for printing in version information.])
AC_SUBST(PACKAGE_BUILD_HOST)
AC_SUBST(PACKAGE_BUILD_HOST_OS)
AC_SUBST(PACKAGE_BUILD_DATE)
AC_CONFIG_FILES([Makefile
                 src/Makefile])
AC_OUTPUT

# Print summary.
AC_MSG_RESULT([ ])
AC_MSG_RESULT([Summary:])
AC_MSG_RESULT([ ])
AC_MSG_RESULT([Build OS:            $PACKAGE_BUILD_HOST_OS])
AC_MSG_RESULT([Build host:          $PACKAGE_BUILD_HOST])
AC_MSG_RESULT([Install dir:         $prefix])
AC_MSG_RESULT([C++ compiler:        $PACKAGE_COMPILER])
AC_MSG_RESULT([CXXFLAGS =           $CXXFLAGS])
AC_MSG_RESULT([LDFLAGS =            $LDFLAGS])
AC_MSG_RESULT([LIBS =               $LIBS])
AC_MSG_RESULT([ ])
