# Installation

For quick installation instructions for OS X and Ubuntu, see the
[BioLite homepage](https://bitbucket.org/caseywdunn/biolite).
This file has more detailed instructions for installation on other platforms
or for developers.

## Prerequisites

To compile biolite-tools, you must at a minimum have a C++ compiler that
supports the TR1 standard, e.g.:

* gcc 4.4 (CentOS 6.3)
* gcc 4.8 (Ubuntu 13.04)
* clang 5.1 (OS X 10.9)

## Installing from the tarball or git repo

biolite-tools uses the standard GNU autoconf method of installation:

    ./configure
    make install

For an alternate install path, specify a prefix with:

    ./configure --prefix=...
    make install

If you are installing biolite-tools directly from the git repo, you first need to
generate the configure script (which requires that you have autoconf/automake
installed):

    cd tools
    ./autogen.sh
    ./configure
    make install

## Generating a tarball from the git repo

Create a tarball for biolite-tools with GNU autoconf:

    cd tools
    ./autogen.sh
    ./configure
    make dist-gzip

