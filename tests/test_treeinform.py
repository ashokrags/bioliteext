from biolite.workflows import phylogeny as phy
import os
from ete3 import Tree
from collections import defaultdict

# workflows.phylogeny.identify_candidate_variants

# code will not work unless threshold is set above 0.33
def test_tree1():
    test1=["((A@0:0.2, A@1:0.2):0.01,A@2:0.01):1;"]

    # yield candidates[species]
    candidates=phy.identify_candidate_variants(test1, 0.05)
    assert(not list(candidates))

    candidates=phy.identify_candidate_variants(test1, 0.41)
    for i in candidates:
        assert(i==set([0,1]))

def test_tree2():
    test2=["((B@0:0.2, A@1:0.2):0.1,A@2:0.4):1;"]

    candidates=phy.identify_candidate_variants(test2, 0.45)
    assert(not list(candidates))

    candidates=phy.identify_candidate_variants(test2, 0.95)
    for i in candidates:
        assert(i==set([1,2]))

def test_tree3():
    test3=["(((((A@0:0.01,A@1:0.01):0.01,B@3:0.01):0.01,A@2:0.01):0.01,B@4:0.01):0.8,(B@5:0.6,C@8:0.4)):1;", "((A@9:0.005, B@10:0.005):0.005,(A@11:0.005,B@12:0.007):0.005):1;","((A@13:0.05, B@14:0.05):0.05,(A@15:0.05,B@16:0.07):0.05):1;", "((B@17:0.02,C@18:0.02):0.01,C@19:0.04):1;"]
    candidates=phy.identify_candidate_variants(test3, 0.1)
    c=0
    for i in candidates:
       if c==0: assert(i==set([0,1,2]))
       elif c==1: assert(i==set([3,4]))
       elif c==2: assert(i==set([9,11]))
       elif c==3: assert(i==set([10,12]))
       elif c==4: assert(i==set([18,19]))
       c=c+1
